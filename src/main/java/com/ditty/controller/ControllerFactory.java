/**
 * 
 */
package com.ditty.controller;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author dingnate
 *
 */
public class ControllerFactory {
	private static ConcurrentHashMap<Class<?>, Object> conrollers = new ConcurrentHashMap<Class<?>, Object>();

	public static Object getController(Class<?> clazz) throws Exception {
		Object object = conrollers.get(clazz);
		if (object != null) {
			return object;
		}
		object = clazz.newInstance();
		Object preObject = conrollers.putIfAbsent(clazz, object);
		if (preObject != null) {
			return preObject;
		}
		return object;
	}
}
