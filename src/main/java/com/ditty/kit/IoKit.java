package com.ditty.kit;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 流工具类
 * 
 * @author dingnate
 *
 */
public class IoKit {
	IoKit() {
	}

	public static void closeQuiet(Closeable c) {
		if (c == null) {
			return;
		}
		try {
			c.close();
		} catch (IOException e) {
			//do nothing
		}
	}

	/**
	 * 拷贝流
	 * 
	 * @param is InputStream
	 * @param os OutputStream
	 * @return
	 * @throws IOException
	 */
	public static long copy(InputStream is, OutputStream os) throws IOException {
		return copy(is, os, new byte[1024]);
	}

	/**
	 * 拷贝流
	 * 
	 * @param is InputStream
	 * @param os OutputStream
	 * @param buf 缓存
	 * @return
	 * @throws IOException
	 */
	public static long copy(InputStream is, OutputStream os, byte[] buf) throws IOException {
		long count = 0;
		int len = 0;
		while (-1 != (len = is.read(buf))) {
			os.write(buf, 0, len);
			count += len;
		}
		return count;
	}
}
