/**
 * 
 */
package com.ditty.interceptor;

import com.ditty.invoke.action.ActionInvoker;

/**
 * @author dingnate
 *
 */
public interface IActionInterceptor {
	/**
	 * interceptor
	 * 
	 * @param invoker
	 */
	void interceptor(ActionInvoker invoker);
}
