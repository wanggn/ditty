/**
 * 
 */
package com.ditty.wrap;

import java.util.Map;

/**
 * @author dingnate
 *
 */
public interface IHttpRequestWrapper {
	/**
	 * 获取headers
	 * @return
	 */
	Map<String, String> headers();

	/**
	 * 获取method
	 * @return
	 */
	String method();

	/**
	 * 获取uri
	 * @return
	 */
	String uri();
	
	/**
	 * 获取content
	 * @return
	 */
	byte[] content();
	
}
