/**
 * 
 */
package com.ditty.wrap;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import com.ditty.kit.HttpKit;
import com.ditty.kit.IoKit;

/**
 * @author dingnate
 *
 */
public class HttpResponseWrapper implements IHttpResponseWrapper {
	private Map<String, String> headers = new HashMap<String, String>();
	private int statusCode = HttpKit.STATUS_200;
	private ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

	@Override
	public Map<String, String> headers() {
		return headers;
	}

	@Override
	public int getStatusCode() {
		return statusCode;
	}

	@Override
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	@Override
	public void write(byte[] bytes) throws IOException {
		outputStream.write(bytes);
	}

	@Override
	public void write(InputStream is) throws IOException {
		IoKit.copy(is, outputStream);
	}

	/**
	 * @return the outputStream
	 */
	@Override
	public final OutputStream getOutputStream() {
		return outputStream;
	}

	@Override
	public byte[] content() {
		return outputStream.toByteArray();
	}
}
