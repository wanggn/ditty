package com.ditty.common;

/**
 * @author dingnate
 *
 * @param <T>
 */
public interface IFilter<T> {
	/**
	 * 接受返回true 否则返回false
	 * @param t
	 * @return
	 */
	boolean accept(T t);
}