/**
 * 
 */
package com.ditty.serialize;

/**
 * @author dingnate
 *
 */
public interface ISerializeConvertor extends ISerializer, IDeserializer {

}
