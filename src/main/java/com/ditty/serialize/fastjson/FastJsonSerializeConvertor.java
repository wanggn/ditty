/**
 * 
 */
package com.ditty.serialize.fastjson;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.ditty.kit.CharsetKit;
import com.ditty.serialize.ISerializeConvertor;

/**
 * @author dingnate
 *
 */
public class FastJsonSerializeConvertor implements ISerializeConvertor {
	@Override
	public byte[] serialize(Object object) {
		return JSON.toJSONStringWithDateFormat(object, "yyyy-MM-dd HH:mm:ss.SSS").getBytes(CharsetKit.DEFAULT_CHARSET);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T deserialize(byte[] bytes, Class<T> clazz) {
		if (clazz.equals(String.class)) {
			return (T) new String(bytes, CharsetKit.DEFAULT_CHARSET);
		}
		if (clazz == Byte[].class || clazz == byte[].class) {
			return (T) bytes;
		}
		return JSON.parseObject(new String(bytes, CharsetKit.DEFAULT_CHARSET), clazz);
	}

	@Override
	public <T> List<T> deserializeArray(byte[] bytes, Class<T> clazz) {
		return JSON.parseArray(new String(bytes, CharsetKit.DEFAULT_CHARSET), clazz);
	}
}
