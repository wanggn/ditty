/**
 * 
 */
package com.ditty.serialize;

import java.util.List;

/**
 * @author dingnate
 *
 */
public interface IDeserializer {
	/**
	 * deserialize Object
	 * @param bytes
	 * @param clazz
	 * @return
	 */
	<T> T deserialize(byte[] bytes, Class<T> clazz);

	/**
	 * 
	 * deserialize Object集合
	 * @param bytes
	 * @param clazz
	 * @return
	 */
	<T> List<T> deserializeArray(byte[] bytes, Class<T> clazz);
}
