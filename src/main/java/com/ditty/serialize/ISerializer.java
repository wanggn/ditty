/**
 * 
 */
package com.ditty.serialize;


/**
 * @author dingnate
 *
 */
public interface ISerializer {
	/**
	 * serialize
	 * @param object
	 * @return
	 */
	byte[] serialize(Object object);
}
