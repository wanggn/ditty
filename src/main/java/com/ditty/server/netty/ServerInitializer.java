package com.ditty.server.netty;

import com.ditty.kit.JdkKit;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpContentCompressor;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.stream.ChunkedWriteHandler;

/**
 * @author dingnate
 *
 */
public class ServerInitializer extends ChannelInitializer<SocketChannel> {

	@Override
	public void initChannel(SocketChannel ch) {
		ChannelPipeline p = ch.pipeline();
		p.addLast(new HttpServerCodec());
		if (JdkKit.isGteVersion(JdkKit.V_1_7)) {
			p.addLast(new HttpContentCompressor());
		}
		p.addLast(new HttpObjectAggregator(1048576));
		p.addLast(new ChunkedWriteHandler());
		// http请求根处理器
		p.addLast(new ServerHandler());
	}
}
