package com.ditty.server.netty;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.AsciiString;

import java.util.HashMap;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ditty.handler.Handlers;
import com.ditty.invoke.handler.HandlerInvoker;
import com.ditty.kit.CharsetKit;
import com.ditty.wrap.HttpRequestWrapper;
import com.ditty.wrap.HttpResponseWrapper;

/**
 * @author dingnate
 *
 */
public class ServerHandler extends ChannelInboundHandlerAdapter {

	private static transient final Logger LOG = LoggerFactory.getLogger(ServerHandler.class);

	public ServerHandler() {
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) {
		ctx.flush();
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		try {
			if (msg instanceof FullHttpRequest) {
				FullHttpRequest req = (FullHttpRequest) msg;
				if (HttpUtil.is100ContinueExpected(req)) {
					ctx.write(new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.CONTINUE));
				}
				//封装请求和响应
				HttpRequestWrapper httpRequestWrapper = buildRequestWraper(req);
				HttpResponseWrapper httpResponseWrapper = new HttpResponseWrapper();
				//处理请求
				handle(httpRequestWrapper, httpResponseWrapper);
				//建造netty响应
				FullHttpResponse response = buildResponse(httpResponseWrapper);
				boolean keepAlive = HttpUtil.isKeepAlive(req);
				if (!keepAlive) {
					ctx.write(response).addListener(ChannelFutureListener.CLOSE);
				} else {
					response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);
					ctx.write(response);
				}
			}
		} catch (Exception e) {
			ctx.write(new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.INTERNAL_SERVER_ERROR, Unpooled.wrappedBuffer(e.toString()
					.getBytes(CharsetKit.DEFAULT_CHARSET))));
			LOG.error("http error!", e);
		}
	}

	private void handle(HttpRequestWrapper httpRequestWrapper, HttpResponseWrapper httpResponseWrapper) {
		new HandlerInvoker(Handlers.me().getHead(), httpRequestWrapper, httpResponseWrapper).handle();
	}

	/**
	 * @param req
	 * @return
	 */
	private HttpRequestWrapper buildRequestWraper(FullHttpRequest req) {
		HashMap<String, String> headersMap = new HashMap<String, String>(16);
		for (Entry<String, String> entry : req.headers()) {
			headersMap.put(entry.getKey(), entry.getValue());
		}
		byte[] content = new byte[req.content().readableBytes()];
		req.content().readBytes(content);
		return new HttpRequestWrapper(req.method().name(), req.uri(), headersMap, content);
	}

	private FullHttpResponse buildResponse(HttpResponseWrapper httpResponseWrapper) {
		FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1,
				HttpResponseStatus.valueOf(httpResponseWrapper.getStatusCode()), Unpooled.wrappedBuffer(httpResponseWrapper.content()));
		HttpHeaders headers = response.headers();
		headers.set(HttpHeaderNames.TRANSFER_ENCODING, HttpHeaderValues.CHUNKED);
		headers.set(HttpHeaderNames.CONTENT_TYPE, new AsciiString("application/json; charset=utf-8"));
		for (Entry<String, String> entry : httpResponseWrapper.headers().entrySet()) {
			headers.set(entry.getKey(), entry.getValue());
		}
		return response;
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		ctx.close();
		LOG.error("ctx close!", cause);
	}
}
