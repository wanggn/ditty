/**
 * 
 */
package com.ditty.server;

/**
 * @author dingnate
 */
public interface IServerFactory {

	/**
	 * 获取Server
	 * @return
	 */
	IServer getServer();

	/**
	 * 获取Server
	 * @param serverType
	 * @return
	 */
	IServer getServer(ServerType serverType);
}
