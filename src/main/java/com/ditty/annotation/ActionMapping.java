/**
 * 
 */
package com.ditty.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.ditty.kit.HttpKit;

/**
 * httpMethod值参考:<br>
 * {@linkplain HttpKit#METHOD_GET}<br>
 * {@linkplain HttpKit#METHOD_POST}<br>
 * {@linkplain HttpKit#METHOD_PUT}<br>
 * {@linkplain HttpKit#METHOD_DELETE}<br>
 * @author dingnate
 *
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ActionMapping {
	String actionKey() default "";

	String[] argNames() default {};

	String httpMethod() default "";
}
