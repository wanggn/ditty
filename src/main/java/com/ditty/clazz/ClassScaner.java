/**
 * 
 */
package com.ditty.clazz;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import com.ditty.common.IFilter;
import com.ditty.kit.ClassLoaderKit;

/**
 * @author dingnate
 *
 */
public class ClassScaner {
	private static final ClassScaner ME = new ClassScaner();
	private final Collection<String> jarPrefixs = new ArrayList<String>();
	private final Collection<String> classPrefixs = new ArrayList<String>();

	ClassScaner() {
	}

	public static ClassScaner me() {
		return ME;
	}

	public void scanClass() {
		ClassLoaderKit.scanClass(new IFilter<String>() {
			@Override
			public boolean accept(String jarname) {
				if (jarPrefixs.isEmpty()) {
					return true;
				}
				for (String excludeJar : jarPrefixs) {
					if (jarname.startsWith(excludeJar)) {
						return true;
					}
				}
				return false;
			}
		}, new IFilter<String>() {
			@Override
			public boolean accept(String classname) {
				if (classPrefixs.isEmpty()) {
					return true;
				}
				for (String includeClass : classPrefixs) {
					if (classname.startsWith(includeClass)) {
						return true;
					}
				}
				return false;
			}
		}, new ActionMappingClassHandler());
	}

	/**
	 * 添加扫描过滤的jar包名前缀
	 * @param prefixs
	 * @return
	 */
	public final ClassScaner addJarPrefixs(String... prefixs) {
		jarPrefixs.addAll(Arrays.asList(prefixs));
		return this;
	}

	/**
	 * @return the includeClasses
	 */
	/**
	 * 添加扫描过滤的类名前缀
	 * 
	 * @param prefixs
	 * @return
	 */
	public final ClassScaner addClassPrefixs(String... prefixs) {
		this.classPrefixs.addAll(Arrays.asList(prefixs));
		return this;
	}
}
