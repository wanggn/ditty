/**
 * 
 */
package com.ditty;

import com.ditty.clazz.ClassScaner;
import com.ditty.handler.ActionHandler;
import com.ditty.handler.ContextPathHandler;
import com.ditty.handler.Handlers;
import com.ditty.handler.ResourceHandler;
import com.ditty.interceptor.Interceptors;
import com.ditty.kit.StrKit;
import com.ditty.kit.ValueKit;
import com.ditty.router.Routers;

/**
 * @author dingnate
 *
 */
public class HttpConfiguration {
	public static final int DEFAULT_PORT = Integer.valueOf(System.getProperty("server.port", "8090"));
	public static final String ARG_SEPARATOR = "-";
	private static HttpConfiguration ME = new HttpConfiguration();
	private final Routers ROUTERS = Routers.me();
	private final Interceptors INTERCEPTORS = Interceptors.me();
	private final Handlers HANDLERS = Handlers.me();
	private final ClassScaner CLASSCANER = ClassScaner.me();
	private String contextPath = "";
	private String webDir = "";
	private String encoding = "UTF-8";
	private int soBacklog;

	protected HttpConfiguration() {
	}

	public static HttpConfiguration me() {
		return ME;
	}

	public void config() {
		config(CLASSCANER);
		config(HANDLERS);
		config(INTERCEPTORS);
		config(ROUTERS);
	}

	protected void config(ClassScaner scaner) {
		// TODO add the scaner jar prefixs or class prefixs here
	}

	protected void config(Routers routers) {
		// TODO add router here
	}

	protected void config(Interceptors interceptors) {
		// TODO add interceptor here
	}

	private void config(Handlers handlers) {
		handlers.addHandler(new ContextPathHandler(contextPath));
		handlers.addHandler(new ResourceHandler());
		handlers.addHandler(new ActionHandler());
	}

	/**
	 * @return the contextPath
	 */
	public final String getContextPath() {
		return contextPath;
	}

	/**
	 * @param contextPath the contextPath to set
	 * @return 
	 */
	public final HttpConfiguration setContextPath(String contextPath) {
		if (StrKit.isNotBlank(contextPath) || !ValueKit.STR_SLASH.equalsIgnoreCase(contextPath)) {
			if (contextPath.endsWith(ValueKit.STR_SLASH)) {
				contextPath = contextPath.substring(0, contextPath.length() - 1);
			}
			this.contextPath = contextPath;
		}
		return this;
	}

	/**
	 * @return the webDir
	 */
	public final String getWebDir() {
		return webDir;
	}

	/**
	 * @param webDir the webDir to set
	 * @return 
	 */
	public final HttpConfiguration setWebDir(String webDir) {
		if (webDir.endsWith(ValueKit.STR_SLASH)) {
			webDir = webDir.substring(0, webDir.length() - 1);
		}
		this.webDir = webDir;
		return this;
	}

	/**
	 * @return the encoding
	 */
	public final String getEncoding() {
		return encoding;
	}

	/**
	 * @param encoding the encoding to set
	 * @return 
	 */
	public final HttpConfiguration setEncoding(String encoding) {
		this.encoding = encoding;
		return this;
	}

	/**
	 * @return the routers
	 */
	public final Routers getRouters() {
		return ROUTERS;
	}

	/**
	 * @return the interceptors
	 */
	public final Interceptors getInterceptors() {
		return INTERCEPTORS;
	}

	/**
	 * @return the class scaner
	 */
	public final ClassScaner getClassSaner() {
		return CLASSCANER;
	}

	/**
	 * @return the handlers
	 */
	public final Handlers getHandlers() {
		return HANDLERS;
	}

	/**
	 * @param me the me to set
	 */
	public static final void setMe(HttpConfiguration me) {
		ME = me;
	}

	/**
	 * @return the soBacklog
	 */
	public final int getSoBacklog() {
		return soBacklog;
	}

	/**
	 * @param soBacklog the soBacklog to set
	 * @return 
	 */
	public final HttpConfiguration setSoBacklog(int soBacklog) {
		this.soBacklog = soBacklog;
		return this;
	}
}