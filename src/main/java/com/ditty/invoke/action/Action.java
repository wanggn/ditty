/**
 * 
 */
package com.ditty.invoke.action;

import java.lang.reflect.Method;

import com.ditty.common.LinkedObject;
import com.ditty.interceptor.AbstractActionIntercptor;

/**
 * @author dingnate
 *
 */
public class Action extends LinkedObject<Action> {
	String httpMethod;
	Class<?> controllerClass;
	Method method;
	String[] argNames;
	AbstractActionIntercptor interceptor;

	public Action() {
	}

	/**
	 * @param httpMethod
	 * @param controllerClass
	 * @param method
	 * @param argNames
	 * @param interceptor
	 */
	public Action(String httpMethod, Class<?> controllerClass, Method method, String[] argNames, AbstractActionIntercptor interceptor) {
		super();
		this.httpMethod = httpMethod;
		this.controllerClass = controllerClass;
		this.method = method;
		this.argNames = argNames;
		this.interceptor = interceptor;
	}

	/**
	 * @return the interceptor
	 */
	public final AbstractActionIntercptor getInterceptor() {
		return interceptor;
	}

	/**
	 * @param interceptor the interceptor to set
	 * @return 
	 */
	public final Action setInterceptor(AbstractActionIntercptor interceptor) {
		this.interceptor = interceptor;
		return this;
	}

	/**
	 * @return the method
	 */
	public final Method getMethod() {
		return method;
	}

	/**
	 * @param method the method to set
	 * @return 
	 */
	public final Action setMethod(Method method) {
		this.method = method;
		return this;
	}

	/**
	 * @return the controllerClass
	 */
	public final Class<?> getControllerClass() {
		return controllerClass;
	}

	/**
	 * @param controllerClass the controllerClass to set
	 * @return 
	 */
	public final Action setControllerClass(Class<?> controllerClass) {
		this.controllerClass = controllerClass;
		return this;
	}

	/**
	 * @return the argNames
	 */
	public final String[] getArgNames() {
		return argNames;
	}

	/**
	 * @param argNames the argNames to set
	 * @return 
	 */
	public final Action setArgNames(String[] argNames) {
		this.argNames = argNames;
		return this;
	}

	/**
	 * @return the httpMethod
	 */
	public final String getHttpMethod() {
		return httpMethod;
	}

	/**
	 * @param httpMethod the httpMethod to set
	 * @return 
	 */
	public final Action setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
		return this;
	}
}
