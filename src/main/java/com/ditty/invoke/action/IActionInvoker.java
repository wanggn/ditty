/**
 * 
 */
package com.ditty.invoke.action;

/**
 * @author dingnate
 *
 */
public interface IActionInvoker {
	/**
	 * inovke
	 * @return 
	 */
	IActionInvoker inovke();
	/**
	 * 获取returnValue
	 * @return
	 */
	<T> T getReturnValue();
}
